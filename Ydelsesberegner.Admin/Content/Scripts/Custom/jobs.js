﻿//jQuery DataTables - Configuration
var table = $("#jobs-table").DataTable({
    "pageLength": 25,
    "ajax": {
        "url": "/api/JobsApi/GetJobs",
        "dataSrc": "",
        "type": "GET",
        "data": function (d) {
            d.applicationId = getApplicationId();
        }
    },
    "columns": [
        { data: "JobTitle" },
        //{ data: "DisplayName" },
        //{ data: "MonthlyWorkHours", render: $.fn.dataTable.render.number(".", ",", 2) },
        //{ data: "MonthlyRate", render: $.fn.dataTable.render.number(".", ",", 2, "", " kr.") },
        //{ data: "HourlyRate", render: $.fn.dataTable.render.number(".", ",", 2, "", " kr.") },
        {
            render: function (data, type, row) {
                return "<a href=\"/Jobs/Edit/" + row.JobId + "\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-pencil\"></span></a> " +
                       "<a href=\"#\" data-job-id=\"" + row.JobId + "\" class=\"btn btn-default btn-sm job-remove\"><span class=\"glyphicon glyphicon-trash\"></span></a>";
            },
            width: "10%"
        }
    ],
    "language": {
        "url": "/Content/Scripts/Libraries/datatables.danish.json"
    }
});

/* Formatting function for row details */
//function format(d) {
//    var conditions = d.Condition !== null && d.Condition.length > 0 ? getConditions(d.Condition) : "<tr><td>Der findes ingen betingelser for at få vist ydelsestypen</td></tr>";
    
//    // `d` is the original data object for the row
//    return "<div class=\"container\">" +
//                "<ul class=\"nav nav-tabs\">" +
//                    "<li class=\"active\"><a href=\"#conditions_" + d.SlideId + "\" data-toggle=\"tab\">Betingelser for at få vist ydelsestype</a></li>" +
//                "</ul>" +
//                "<div class=\"tab-content clearfix\">" +
//                    "<div class=\"tab-pane active\" id=\"conditions_" + d.SlideId + "\">" +
//                        "<table class=\"table\">" +
//                            "<thead><tr>" +
//                            "<th>Spørgsmål</th>" +
//                            "<th>Svarmulighed</th>" +
//                            "</tr></thead>" +
//                            "<tbody>" + conditions + "</tbody>" +
//                        "</table>" +
//                    "</div>" +
//                "</div>" +
//             "</div>";
//}

//Event listener for opening and closing details
//$("#benefit-types-table tbody").on("click", "td.details-control", function () {
//    var tr = $(this).closest("tr");
//    var row = table.row(tr);

//    if (row.child.isShown()) {
//        // This row is already open - close it
//        row.child.hide();
//        tr.removeClass("shown");
//    }
//    else {
//        // Open this row
//        row.child(format(row.data())).show();
//        tr.addClass("shown");
//    }
//});

//Event listener for remove click
$("#jobs-table").on("click", "a.job-remove", function (e) {
    e.preventDefault();

    var jobId = $(this).data("job-id");

    console.log("remove", jobId);

    deleteJob(jobId);

    table.ajax.reload(null, false);
});

//Event listener for application change
$("#Applications").change(function () {
    table.ajax.reload(null, false);
});

//Event listener for Create Job click
$("#create-job").click(function () {
    var appId = $("#Applications").val();
    var url = "/Jobs/Create";

    window.location.href = url + "?applicationId=" + appId;
});

//Helper methods
function getApplicationId() {
    return $("#Applications").val().length === 0 ? 1 : $("#Applications").val();
}

function deleteJob(jobId) {
    jQuery.ajax({
        type: "DELETE",
        url: "/api/JobsApi/DeleteJob?id=" + jobId,
        async: false,
        success: function (response) {
            console.log("delete job success", response);
        },
        error: function (response) {
            console.log("Der opstod en fejl under sletning af jobbet.", response);
        }
    });
}