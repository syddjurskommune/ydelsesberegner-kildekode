﻿//jQuery DataTables - Configuration
var table = $("#slides-table").DataTable({
    "pageLength": 50,
    "ajax": {
        "url": "/api/SlidesApi/GetSlides",
        "dataSrc": "",
        "type": "GET",
        "data": function (d) {
            d.applicationId = getApplicationId();
        }
    },
    "columns": [
        { className: "details-control", orderable: false, defaultContent: "", width: "5%" },
        { className: "reorder", defaultContent: "<span class=\"glyphicon glyphicon-move\"></span>" },
        { data: "SlideName" },
        { data: "SlideHeadline" },
        {
            render: function (data, type, row) {
                return "<a href=\"/Slides/Edit/" + row.SlideId + "\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-pencil\"></span></a> " +
                       "<a href=\"#\" data-slide-id=\"" + row.SlideId + "\" class=\"btn btn-default btn-sm slide-remove\"><span class=\"glyphicon glyphicon-trash\"></span></a>";
            },
            width: "10%"
        }
    ],
    "rowReorder": {
        selector: ".reorder",
        dataSrc: "SlideOrderIndex"
    },
    "select": true,
    "language": {
        "url": "/Content/Scripts/Libraries/datatables.danish.json"
    }
});

/* Formatting function for row details */
function format(d) {
    var conditions = d.Condition !== null && d.Condition.length > 0 ? getConditions(d.Condition) : "<tr><td>Der findes ingen betingelser for at få vist slide</td></tr>";
    var questionAndAnswers = d.Question !== null ? "<tr><td>" + d.Question.QuestionSystemName + "</td><td>" + Object.keys(d.Question.Answer).map(function (k) { return d.Question.Answer[k].AnswerText }).join(", ") + "</td></tr>" : "<tr><td>Der findes ingen spørgsmål og svarmuligheder</td></tr>";

    // `d` is the original data object for the row
    return "<div class=\"container\">" +
            "<ul class=\"nav nav-tabs\">" +
                "<li class=\"active\"><a href=\"#conditions_" + d.SlideId + "\" data-toggle=\"tab\">Betingelser for at få vist slide</a></li>" +
                "<li><a href=\"#questions-and-answers_" + d.SlideId + "\" data-toggle=\"tab\">Spørgsmål og svar</a></li>" +
            "</ul>" +
            "<div class=\"tab-content clearfix\">" +
                "<div class=\"tab-pane active\" id=\"conditions_" + d.SlideId + "\">" +
                    "<table class=\"table\">" +
                        "<thead><tr>" +
                            "<th style=\"width: 50%\">Spørgsmål</th>" +
                            "<th style=\"width: 50%\">Svarmulighed</th>" +
                        "</tr></thead>" +
                        "<tbody>" + conditions + "</tbody>" +
                    "</table>" +
                "</div>" +
                "<div class=\"tab-pane\" id=\"questions-and-answers_" + d.SlideId + "\">" +
                    "<table class=\"table\">" +
                        "<thead><tr>" +
                            "<th style=\"width: 50%\">Spørgsmål</th>" +
                            "<th style=\"width: 50%\">Svarmuligheder</th>" +
                        "</tr></thead>" +
                        "<tbody>" + questionAndAnswers + "</tbody>" +
                    "</table>" +
                "</div" +
            "</div>" +
        "</div>";
}

//Event listener for opening and closing details
$("#slides-table tbody").on("click", "td.details-control", function () {
    var tr = $(this).closest("tr");
    var row = table.row(tr);

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass("shown");
    }
    else {
        // Open this row
        row.child(format(row.data())).show();
        tr.addClass("shown");
    }
});

//Event listener for remove click
$("#slides-table").on("click", "a.slide-remove", function (e) {
    e.preventDefault();

    var slideId = $(this).data("slide-id");

    console.log("remove", slideId);

    deleteSlide(slideId);

    table.ajax.reload(null, false);
});

//Event listener for reordering rows
table.on("row-reorder", function (e, diff, edit) {
    //var result = "Reorder started on row: " + edit.triggerRow.data().SlideName + "<br>";

    for (var i = 0, ien = diff.length; i < ien; i++) {
        var rowData = table.row(diff[i].node).data();

        var slideId = parseInt(rowData.SlideId);
        var newSlideOrderIndex = parseInt(diff[i].newData);

        //result += rowData.SlideName + "(" + slideId + ") updated to be in position " + diff[i].newData + " (was " + diff[i].oldData + ")<br>";

        updateSlideOrderIndex(slideId, newSlideOrderIndex);
    }

    //$("#slideOrderResult").html("Event result:<br>" + result);

    table.ajax.reload(null, false);
});

//Event listener for application change
$("#Applications").change(function () {
    table.ajax.reload(null, false);
});

//Event listener for Create Slide click
$("#create-slide").click(function () {
    var appId = $("#Applications").val();
    var url = "/Slides/Create";

    window.location.href = url + "?applicationId=" + appId;
});

//Helper methods
function getApplicationId() {
    return $("#Applications").val().length === 0 ? 1 : $("#Applications").val();
}

function getConditions(conditions) {
    var data = "";

    $.each(conditions, function (index, object) {
        data += "<tr><td>" + object.Question.QuestionSystemName + "</td><td>" + object.Answer.AnswerText + "</td></tr>";
    });

    return data;
}

function updateSlideOrderIndex(slideId, newSlideOrderIndex) {
    jQuery.ajax({
        type: "PUT",
        url: "/api/SlidesApi/UpdateSlideOrderIndex?slideId=" + slideId + "&newSlideOrderIndex=" + newSlideOrderIndex,
        async: false,
        success: function (response) {
            //console.log("slide order update | success", response);
        },
        error: function (response) {
            console.log("Der opstod en fejl under sortering af slides.", response);
        }
    });
}

function deleteSlide(slideId) {
    jQuery.ajax({
        type: "DELETE",
        url: "/api/SlidesApi/DeleteSlide?id=" + slideId,
        async: false,
        success: function (response) {
            //console.log("slide order update | success", response);
        },
        error: function (response) {
            console.log("Der opstod en fejl under sletning af slidet.", response);
        }
    });   
}