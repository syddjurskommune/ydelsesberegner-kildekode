﻿//jQuery DataTables - Configuration
var table = $("#applications-table").DataTable({
    "pageLength": 25,
    "ajax": {
        "url": "/api/ApplicationsApi/GetApplications",
        "dataSrc": "",
        "type": "GET"
    },
    "columns": [
        { data: "Name" },
        { data: "Url" },
        {
            render: function (data, type, row) {
                return "<a href=\"/Applications/Edit/" + row.ApplicationId + "\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-pencil\"></span></a>";
            },
            width: "10%"
        }
    ],
    "language": {
        "url": "/Content/Scripts/Libraries/datatables.danish.json"
    }
});

//Event listener for remove click
$("#applications-table").on("click", "a.application-remove", function (e) {
    e.preventDefault();

    var applicationId = $(this).data("application-id");

    console.log("remove", applicationId);

    deleteApplication(applicationId);

    table.ajax.reload(null, false);
});

//Event listener for Create application click
$("#create-application").click(function () {
    var url = "/ApplicationsApi/Create";

    window.location.href = url;
});

function deleteApplication(applicationId) {
    jQuery.ajax({
        type: "DELETE",
        url: "/api/ApplicationsApi/DeleteApplication?id=" + applicationId,
        async: false,
        error: function (response) {
            console.log("Der opstod en fejl under sletning af applikationen.", response);
        }
    });
}