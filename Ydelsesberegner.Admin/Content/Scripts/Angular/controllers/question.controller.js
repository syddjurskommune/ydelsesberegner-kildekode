﻿app.controller("questionController", function ($scope, $log, $http, answers) {
    $scope.answers = answers;

    $scope.add = function () {
        $scope.answers.push({});
    };

    $scope.remove = function (index) {
        $scope.answers.splice(index, 1);
    }
});