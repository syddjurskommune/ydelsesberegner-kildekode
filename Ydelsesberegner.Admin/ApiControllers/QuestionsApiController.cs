﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.ApiControllers
{
    public class QuestionsApiController : ApiController
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public List<QuestionViewModel> GetQuestions(int applicationId)
        {
            var questions = Mapper.Map<List<Question>, List<QuestionViewModel>>(db.Question.Where(a => a.Slide.ApplicationId == applicationId).ToList());

            return questions;
        }

        public List<AnswerViewModel> GetAnswers(int questionId)
        {
            var answers = Mapper.Map<List<Answer>, List<AnswerViewModel>>(db.Answer.Where(a => a.QuestionId == questionId).ToList());

            return answers;
        }

        [ResponseType(typeof(Question))]
        public IHttpActionResult GetQuestion(int id)
        {
            Question question = db.Question.Find(id);

            if (question == null)
            {
                return NotFound();
            }

            return Ok(question);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}