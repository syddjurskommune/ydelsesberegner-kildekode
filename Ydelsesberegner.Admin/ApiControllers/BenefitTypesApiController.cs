﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.ApiControllers
{
    public class BenefitTypesApiController : ApiController
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public List<BenefitTypeViewModel> GetBenefitTypes(int applicationId)
        {
            var benefitTypes = Mapper.Map<List<BenefitType>, List<BenefitTypeViewModel>>(db.BenefitType.Where(b => b.ApplicationId == applicationId).ToList());

            return benefitTypes;
        }

        [ResponseType(typeof(BenefitTypeViewModel))]
        public IHttpActionResult DeleteBenefitType(int id)
        {
            BenefitType benefitType = db.BenefitType.Find(id);
            if (benefitType == null)
            {
                return NotFound();
            }

            db.Condition.RemoveRange(benefitType.Condition);
            db.BenefitType.Remove(benefitType);
            db.SaveChanges();

            var benefitTypeViewModel = Mapper.Map<BenefitTypeViewModel>(benefitType);

            return Ok(benefitTypeViewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}