﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.ApiControllers
{
    public class ApplicationsApiController : ApiController
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public List<ApplicationViewModel> GetApplications()
        {
            var applications = Mapper.Map<List<Application>, List<ApplicationViewModel>>(db.Application.ToList());

            return applications;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}