﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using AutoMapper;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.ApiControllers
{
    public class SlidesApiController : ApiController
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public List<SlideViewModel> GetSlides(int applicationId)
        {
            var slides = Mapper.Map<List<Slide>, List<SlideViewModel>>(db.Slide.Where(s => s.ApplicationId == applicationId).OrderBy(s => s.SlideOrderIndex).ToList());

            return slides;
        }

        [System.Web.Http.HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult UpdateSlideOrderIndex(int slideId, int newSlideOrderIndex)
        {
            var slide = db.Slide.Single(s => s.SlideId == slideId);

            slide.SlideOrderIndex = newSlideOrderIndex;

            db.Entry(slide).State = EntityState.Modified;
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteSlide(int id)
        {
            try
            {
                Slide slide = db.Slide.Find(id);

                if (slide == null)
                {
                    return NotFound();
                }


                db.Attachment.RemoveRange(slide.Attachment);
                db.Condition.RemoveRange(slide.Condition);
                
                foreach (var question in slide.Question.ToList())
                {
                    db.Condition.RemoveRange(question.Condition);
                    db.Answer.RemoveRange(question.Answer);
                    db.Question.Remove(question);
                }

                db.Slide.Remove(slide);
                db.SaveChanges();

                foreach (var existingSlide in db.Slide.Where(s => s.ApplicationId == slide.ApplicationId && s.SlideOrderIndex > slide.SlideOrderIndex).ToList())
                {
                    existingSlide.SlideOrderIndex = existingSlide.SlideOrderIndex - 1;

                    db.Entry(existingSlide).State = EntityState.Modified;
                }

                db.SaveChanges();
                
                return Ok();

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
