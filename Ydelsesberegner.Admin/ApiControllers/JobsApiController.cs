﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Ydelsesberegner.Admin.Controllers;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.ApiControllers
{
    public class JobsApiController : ApiController
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public List<JobViewModel> GetJobs(int? applicationId)
        {
            var jobs = Mapper.Map<List<Job>, List<JobViewModel>>(db.Job.Where(j => j.ApplicationId == applicationId).ToList());

            return jobs;
        }

        [ResponseType(typeof(JobViewModel))]
        public IHttpActionResult DeleteJob(int id)
        {
            Job job = db.Job.Find(id);

            if (job == null)
            {
                return NotFound();
            }

            db.Job.Remove(job);
            db.SaveChanges();

            var jobViewModel = Mapper.Map<JobViewModel>(job);

            return Ok(jobViewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }

        private bool JobExists(int id)
        {
            return db.Job.Count(e => e.JobId == id) > 0;
        }
    }
}