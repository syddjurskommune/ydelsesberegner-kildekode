﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ydelsesberegner.Admin.ViewModels
{
    public class AnswerViewModel
    {
        public int AnswerId { get; set; }

        [Display(Name = "Spørgsmål")]
        public int QuestionId { get; set; }

        [Display(Name = "Svar")]
        [MaxLength(150)]
        public string AnswerText { get; set; }
    }
}