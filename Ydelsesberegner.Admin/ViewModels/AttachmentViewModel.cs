﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ydelsesberegner.Admin.ViewModels
{
    public class AttachmentViewModel
    {
        public int AttachmentId { get; set; }

        [Display(Name = "Slide")]
        [MaxLength(150)]
        [Required]
        public int SlideId { get; set; }

        [Display(Name = "Filnavn")]
        [MaxLength(150)]
        [Required]
        public string FileName { get; set; }

        [Display(Name = "Filtype")]
        [MaxLength(150)]
        [Required]
        public string FileType { get; set; }

        [Display(Name = "Fil")]
        [Required]
        public byte[] FileContent { get; set; }

        public SlideViewModel Slide { get; set; }
    }
}