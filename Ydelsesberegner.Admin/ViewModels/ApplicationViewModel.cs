﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Ydelsesberegner.Admin.ViewModels
{
    public class ApplicationViewModel
    {
        public int ApplicationId { get; set; }

        [Display(Name = "Navn")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Sti til resultat view")]
        [Required]
        public string ResultViewPath { get; set; }

        [Display(Name = "Navn på resultat action")]
        [Required]
        public string ActionName { get; set; }

        [Display(Name = "Sti til layout view")]
        [Required]
        public string Layout { get; set; }

        [Display(Name = "Navn på print action")]
        [Required]
        public string PrintActionName { get; set; }

        [Display(Name = "URL til beregneren")]
        [Required]
        public string Url { get; set; }
    }
}