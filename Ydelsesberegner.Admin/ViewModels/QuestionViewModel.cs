﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ydelsesberegner.Admin.ViewModels
{
    public class QuestionViewModel
    {
        public int QuestionId { get; set; }

        [Display(Name = "Slide")]
        public int SlideId { get; set; }

        [Display(Name = "Spørgsmål - Systemnavn")]
        [MaxLength(150)]
        public string QuestionSystemName { get; set; }

        [Display(Name = "Spørgsmål")]
        [MaxLength(150)]
        public string QuestionText { get; set; }

        [Display(Name = "Svarmuligheder")]
        public ICollection<AnswerViewModel> Answer { get; set; }
    }
}