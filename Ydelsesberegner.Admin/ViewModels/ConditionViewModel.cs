﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ydelsesberegner.Admin.ViewModels
{
    public class ConditionViewModel
    {
        public int ConditionId { get; set; }
        
        [Display(Name = "Spørgsmål")]
        public int QuestionId { get; set; }

        [Display(Name = "Svar")]
        public int AnswerId { get; set; }

        [Display(Name = "Slide")]
        public int? SlideId { get; set; }

        [Display(Name = "Ydelsestype")]
        public int? BenefitTypeId { get; set; }

        [Display(Name = "Spørgsmål")]
        public QuestionViewModel Question { get; set; }

        [Display(Name = "Svar")]
        public AnswerViewModel Answer { get; set; }
    }
}