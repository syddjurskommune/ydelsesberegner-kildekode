﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Ydelsesberegner.Admin.ViewModels
{
    public class BenefitTypeViewModel
    {
        public int BenefitTypeId { get; set; }

        [Display(Name = "Applikation")]
        public int ApplicationId { get; set; }

        [Display(Name = "Ydelsestype")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Navn")]
        public string DisplayName { get; set; }

        [Display(Name = "Tekst")]
        [AllowHtml]
        public string Text { get; set; }

        [Display(Name = "Månedsløn")]
        [Required]
        public decimal MonthlyRate { get; set; }

        [Display(Name = "Antal arbejdstimer månedligt")]
        [Required]
        public decimal MonthlyWorkHours { get; set; }

        [Display(Name = "Maksimal længde på ydelse")]
        public int? MaxLengthMonths { get; set; }

        [Display(Name = "Timeløn")]
        public decimal HourlyRate
        {
            get { return MonthlyWorkHours != 0 && MonthlyRate != 0 ? Math.Round(MonthlyRate / MonthlyWorkHours, 2) : 0; }
            private set { }
        }

        [Display(Name = "Årsløn")]
        public decimal YearlyRate
        {
            get { return MonthlyRate != 0 ? Math.Round(MonthlyRate * 12, 2) : 0; }
            private set { }
        }

        [Display(Name = "Masimal længde - Løn")]
        public decimal MaxLengthRate
        {
            get { return MaxLengthMonths.HasValue ? Math.Round(MonthlyRate * MaxLengthMonths.Value, 2) : 0; }
            private set { }
        }

        [Display(Name = "Betingelser")]
        public ICollection<ConditionViewModel> Condition { get; set; }
    }
}