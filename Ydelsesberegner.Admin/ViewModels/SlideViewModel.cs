﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Ydelsesberegner.Admin.ViewModels
{
    public class SlideViewModel
    {
        public int SlideId { get; set; }

        [Display(Name = "Applikation")]
        public int ApplicationId { get; set; }

        [Display(Name = "Sorteringsnøgle")]
        public int SlideOrderIndex { get; set; }

        [Display(Name = "Slide navn")]
        [MaxLength(150)]
        [Required]
        public string SlideName { get; set; }

        [Display(Name = "Slide overskrift")]
        [MaxLength(150)]
        public string SlideHeadline { get; set; }

        [Display(Name = "Slide tekst")]
        [AllowHtml]
        public string SlideText { get; set; }

        [Display(Name = "Resultat slide")]
        public bool IsResultSlide { get; set; }

        [Display(Name = "Vis Print-knap")]
        public bool DisplayPrintButton { get; set; }

        [Display(Name = "Vis Næste-knap")]
        public bool DisplayNextButton { get; set; }

        [Display(Name = "Vis Gå tilbage til start-knap")]
        public bool DisplayReturnToStartButton { get; set; }

        [Display(Name = "Vis tekstboks")]
        public bool DisplayTextBox { get; set; }

        [Display(Name = "Spørgsmål")]
        public QuestionViewModel Question { get; set; }

        [Display(Name = "Betingelser")]
        public ICollection<ConditionViewModel> Condition { get; set; }

        [Display(Name = "Filer")]
        public IEnumerable<HttpPostedFileBase> Attachment { get; set; }
    }
}