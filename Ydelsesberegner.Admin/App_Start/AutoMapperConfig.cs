﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace Ydelsesberegner.Admin
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Models.Application, ViewModels.ApplicationViewModel>();
                cfg.CreateMap<ViewModels.ApplicationViewModel, Models.Application>();

                cfg.CreateMap<Models.Slide, ViewModels.SlideViewModel>().ForMember(x => x.Question, opt => opt.MapFrom(src => src.Question.FirstOrDefault()))
                                                                        .ForMember(x => x.Attachment, opt => opt.Ignore());

                cfg.CreateMap<ViewModels.SlideViewModel, Models.Slide>().ForMember(x => x.Question, opt => opt.Ignore())
                                                                        .ForMember(x => x.Condition, opt => opt.Ignore())
                                                                        .ForMember(x => x.Attachment, opt => opt.Ignore());

                cfg.CreateMap<Models.Question, ViewModels.QuestionViewModel>();
                cfg.CreateMap<ViewModels.QuestionViewModel, Models.Question>().ForMember(x => x.Answer, opt => opt.Ignore());

                cfg.CreateMap<Models.Answer, ViewModels.AnswerViewModel>();
                cfg.CreateMap<ViewModels.AnswerViewModel, Models.Answer>();

                cfg.CreateMap<Models.Attachment, ViewModels.AttachmentViewModel>();
                cfg.CreateMap<ViewModels.AttachmentViewModel, Models.Attachment>();

                cfg.CreateMap<Models.Condition, ViewModels.ConditionViewModel>();
                cfg.CreateMap<ViewModels.ConditionViewModel, Models.Condition>();

                cfg.CreateMap<Models.BenefitType, ViewModels.BenefitTypeViewModel>();
                cfg.CreateMap<ViewModels.BenefitTypeViewModel, Models.BenefitType>().ForMember(x => x.Condition, opt => opt.Ignore());

                cfg.CreateMap<Models.Job, ViewModels.JobViewModel>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            });
        }
    }
}
