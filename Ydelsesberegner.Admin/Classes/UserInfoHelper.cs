﻿using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ydelsesberegner.Admin.Models;

namespace Ydelsesberegner.Admin.Classes
{
    public static class UserInfoHelper
    {
        public static string GetDisplayName()
        {
            var domain = new PrincipalContext(ContextType.Domain);
            var currentUser = UserPrincipal.FindByIdentity(domain, HttpContext.Current.User.Identity.Name);

            return currentUser == null ? string.Empty : currentUser.DisplayName;
        }
    }
}