﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.Controllers
{
    public class AttachmentsController : Controller
    {
        public static void CreateAttachments(int slideId, IEnumerable<HttpPostedFileBase> files)
        {
            DeleteAttachments(slideId);

            using (var db = new YdelsesberegnerContext())
            {
                foreach (var file in files)
                {
                    MemoryStream target = new MemoryStream();

                    file.InputStream.CopyTo(target);

                    byte[] data = target.ToArray();

                    var attachmentViewModel = new AttachmentViewModel
                    {
                        SlideId = slideId,
                        FileName = file.FileName,
                        FileType = file.ContentType,
                        FileContent = data

                    };

                    var attachment = Mapper.Map<Attachment>(attachmentViewModel);

                    db.Attachment.Add(attachment);
                    db.SaveChanges();
                }

            }
        }

        public ActionResult DownloadAttachment(int attachmentId)
        {
            using (var db = new YdelsesberegnerContext())
            {
                var attachment = db.Attachment.SingleOrDefault(a => a.AttachmentId == attachmentId);

                if (attachment == null)
                {
                    return HttpNotFound();
                }

                byte[] fileBytes = attachment.FileContent;
                string fileName = attachment.FileName;

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
        }

        public ActionResult DeleteAttachment(int attachmentId)
        {
            using (var db = new YdelsesberegnerContext())
            {
                var attachment = db.Attachment.SingleOrDefault(a => a.AttachmentId == attachmentId);

                if (attachment == null) return HttpNotFound();

                db.Attachment.Remove(attachment);
                db.SaveChanges();

                return RedirectToAction("Edit", "Slides", new {id = attachment.SlideId});
            }
        }

        public static void DeleteAttachments(int slideId)
        {
            using (var db = new YdelsesberegnerContext())
            {
                var existingAttachmenets = db.Slide.Single(s => s.SlideId == slideId).Attachment;

                if (existingAttachmenets.Any())
                {
                    db.Attachment.RemoveRange(existingAttachmenets);
                }

                db.SaveChanges();
            }
        }
    }
}
