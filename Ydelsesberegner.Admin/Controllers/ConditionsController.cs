﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.Controllers
{
    public class ConditionsController : Controller
    {
        public static void CreateConditions(int? slideId, int? benefitTypeId, List<ConditionViewModel> conditions)
        {
            DeleteConditions(slideId, benefitTypeId);

            using (var db = new YdelsesberegnerContext())
            {
                foreach (var c in conditions.ToList())
                {
                    var condition = Mapper.Map<Condition>(c);

                    if (slideId.HasValue) { condition.SlideId = slideId; }
                    if (benefitTypeId.HasValue) { condition.BenefitTypeId = benefitTypeId; }

                    db.Condition.Add(condition);
                }

                db.SaveChanges();
            }
        }
        public static void DeleteConditions(int? slideId, int? benefitTypeId)
        {
            using (var db = new YdelsesberegnerContext())
            {
                var existingConditions = slideId.HasValue ? db.Condition.Where(c => c.SlideId == slideId) : db.Condition.Where(c => c.BenefitTypeId == benefitTypeId);

                if (existingConditions.Any())
                {
                    db.Condition.RemoveRange(existingConditions);
                }

                db.SaveChanges();
            }
        }
    }
}