﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.Controllers
{
    public class SlidesController : Controller
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public ActionResult Index(int? applicationId)
        {
            ViewBag.Applications = new SelectList(db.Application.Select(a => new { a.ApplicationId, a.Name }), "ApplicationId", "Name", applicationId ?? (db.Application.FirstOrDefault() != null ? db.Application.First().ApplicationId : 0));

            return View();
        }
        public ActionResult Create(int? applicationId)
        {
            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", applicationId);

            var slide = new SlideViewModel { DisplayNextButton = true };

            return View("SlideForm", slide);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Slide slide_db = db.Slide.Find(id);

            if (slide_db == null) return HttpNotFound();

            SlideViewModel slideViewModel = Mapper.Map<SlideViewModel>(slide_db);

            slideViewModel.Question = Mapper.Map<QuestionViewModel>(slide_db.Question.FirstOrDefault());

            ViewBag.Attachments = Mapper.Map<List<AttachmentViewModel>>(slide_db.Attachment.ToList());
            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", slideViewModel.ApplicationId);

            return View("SlideForm", slideViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SlideId,SlideGUID,ApplicationId,SlideOrderIndex,SlideName,SlideHeadline,SlideText,IsResultSlide,DisplayPrintButton,DisplayNextButton,DisplayReturnToStartButton,DisplayTextBox,Question,Condition,Attachment")] SlideViewModel slideViewModel)
        {
            if (ModelState.IsValid)
            {
                var slide_db = Mapper.Map<Slide>(slideViewModel);

                slide_db.SlideOrderIndex = db.Slide.Any(s => s.ApplicationId == slideViewModel.ApplicationId) ? db.Slide.Where(s => s.ApplicationId == slideViewModel.ApplicationId).Max(s => s.SlideOrderIndex) + 1 : 0;

                db.Slide.Add(slide_db);
                db.SaveChanges();

                slideViewModel.SlideId = slide_db.SlideId;

                if (!string.IsNullOrWhiteSpace(slideViewModel.Question?.QuestionSystemName))
                {
                    CreateOrUpdateQuestion(slide_db.SlideId, slideViewModel.Question);
                }

                if (slideViewModel.Condition != null)
                {
                    ConditionsController.CreateConditions(slide_db.SlideId, null, slideViewModel.Condition.ToList());
                }

                if (slideViewModel.Attachment != null && slideViewModel.Attachment.Any(a => a != null))
                {
                    AttachmentsController.CreateAttachments(slide_db.SlideId, slideViewModel.Attachment);
                }

                return RedirectToAction("Index", new { applicationId = slideViewModel.ApplicationId });
            }

            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", slideViewModel.ApplicationId);

            return View("SlideForm", slideViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SlideId,SlideGUID,ApplicationId,SlideOrderIndex,SlideName,SlideHeadline,SlideText,IsResultSlide,DisplayPrintButton,DisplayNextButton,DisplayReturnToStartButton,DisplayTextBox,Question,Condition,Attachment")] SlideViewModel slideViewModel)
        {
            if (ModelState.IsValid)
            {
                var slide_db = Mapper.Map<Slide>(slideViewModel);

                db.Entry(slide_db).State = EntityState.Modified;
                db.SaveChanges();

                if (!string.IsNullOrWhiteSpace(slideViewModel.Question?.QuestionSystemName))
                {
                    CreateOrUpdateQuestion(slide_db.SlideId, slideViewModel.Question);
                }

                if (slideViewModel.Condition != null)
                {
                    ConditionsController.CreateConditions(slide_db.SlideId, null, slideViewModel.Condition.ToList());
                }
                else
                {
                    ConditionsController.DeleteConditions(slide_db.SlideId, null);
                }

                if (slideViewModel.Attachment != null && slideViewModel.Attachment.Any(a => a != null))
                {
                    AttachmentsController.CreateAttachments(slide_db.SlideId, slideViewModel.Attachment);
                }

                return RedirectToAction("Index", new { applicationId = slideViewModel.ApplicationId });
            }

            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", slideViewModel.ApplicationId);

            return View("SlideForm", slideViewModel);
        }

        protected void CreateOrUpdateQuestion(int slideId, QuestionViewModel questionViewModel)
        {
            var question = Mapper.Map<Question>(questionViewModel);

            question.SlideId = slideId;

            if (questionViewModel.QuestionId != 0)
            {
                db.Entry(question).State = EntityState.Modified;

                foreach (var answerViewModel in questionViewModel.Answer)
                {
                    var answer = Mapper.Map<Answer>(answerViewModel);

                    answer.QuestionId = question.QuestionId;

                    if (answerViewModel.AnswerId != 0)
                    {
                        db.Entry(answer).State = EntityState.Modified;
                    }
                    else
                    {
                        db.Answer.Add(answer);
                    }
                }

                db.SaveChanges();
            }
            else
            {
                db.Question.Add(question);
                db.SaveChanges();

                foreach (var answerViewModel in questionViewModel.Answer)
                {
                    var answer = Mapper.Map<Answer>(answerViewModel);

                    answer.QuestionId = question.QuestionId;

                    db.Answer.Add(answer);
                }

                db.SaveChanges();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
