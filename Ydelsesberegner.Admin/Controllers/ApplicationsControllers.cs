﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.Controllers
{
    public class ApplicationsController : Controller
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View("ApplicationForm");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationId,Name,ResultViewPath,ActionName,Layout,PrintActionName,Url")] ApplicationViewModel applicationViewModel)
        {
            if (ModelState.IsValid)
            {
                var application_db = Mapper.Map<Application>(applicationViewModel);

                db.Application.Add(application_db);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View("ApplicationForm", applicationViewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Application application_db = db.Application.Find(id);

            if (application_db == null)
            {
                return HttpNotFound();
            }

            ApplicationViewModel applicationViewModel = Mapper.Map<ApplicationViewModel>(application_db);

            return View("ApplicationForm", applicationViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicationId,Name,ResultViewPath,ActionName,Layout,PrintActionName,Url")] ApplicationViewModel applicationViewModel)
        {
            if (ModelState.IsValid)
            {
                var application_db = Mapper.Map<Application>(applicationViewModel);

                db.Entry(application_db).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View("ApplicationForm", applicationViewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
