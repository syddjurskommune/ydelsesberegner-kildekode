﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Newtonsoft.Json;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.Controllers
{
    public class JobsController : Controller
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public ActionResult Index(int? applicationId)
        {
            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", applicationId ?? (db.Application.FirstOrDefault() != null ? db.Application.First().ApplicationId : 0));

            return View();
        }

        public ActionResult Create(int? applicationId)
        {
            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", applicationId);

            var jobViewModel = new JobViewModel { MonthlyWorkHours = 160.33M };

            return View("JobForm", jobViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "JobId,ApplicationId,JobTitle,MonthlySalary,MonthlyWorkHours")] JobViewModel jobViewModel)
        {
            if (ModelState.IsValid)
            {
                var job_db = Mapper.Map<Job>(jobViewModel);

                db.Job.Add(job_db);
                db.SaveChanges();

                return RedirectToAction("Index", new { applicationId = job_db.ApplicationId });
            }

            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", jobViewModel.ApplicationId);

            return View("JobForm", jobViewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Job job_db = db.Job.Find(id);

            if (job_db == null)
            {
                return HttpNotFound();
            }

            var jobViewModel = Mapper.Map<JobViewModel>(job_db);

            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", job_db.ApplicationId);

            return View("JobForm", jobViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "JobId,ApplicationId,JobTitle,MonthlySalary,MonthlyWorkHours")] JobViewModel jobViewModel)
        {
            if (ModelState.IsValid)
            {
                var job_db = Mapper.Map<Job>(jobViewModel);

                db.Entry(job_db).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index", new { applicationId = job_db.ApplicationId });
            }

            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", jobViewModel.ApplicationId);

            return View("JobForm", jobViewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
