﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ydelsesberegner.Admin.Models;
using Ydelsesberegner.Admin.ViewModels;

namespace Ydelsesberegner.Admin.Controllers
{
    public class BenefitTypesController : Controller
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public ActionResult Index(int? applicationId)
        {
            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", applicationId ?? (db.Application.FirstOrDefault() != null ? db.Application.First().ApplicationId : 0));

            return View();
        }

        public ActionResult Create(int? applicationId)
        {
            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", applicationId);

            var benefitTypeViewModel = new BenefitTypeViewModel { MonthlyWorkHours = 160.33M };

            return View("BenefitTypeForm", benefitTypeViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BenefitTypeId,ApplicationId,Name,DisplayName,Text,MonthlyRate,MonthlyWorkHours,MaxLengthMonths,HourlyRate,Condition")] BenefitTypeViewModel benefitTypeViewModel)
        {
            if (ModelState.IsValid)
            {
                var benefitType_db = Mapper.Map<BenefitType>(benefitTypeViewModel);

                benefitType_db.Text = benefitType_db.Text == "<p><br></p>" ? null : benefitType_db.Text; //I IE gemmes "<p><br></p>" hvis Tekst-feltet er tomt derfor erstatter vi det med NULL

                db.BenefitType.Add(benefitType_db);
                db.SaveChanges();

                benefitTypeViewModel.BenefitTypeId = benefitType_db.BenefitTypeId;

                if (benefitTypeViewModel.Condition != null)
                {
                    ConditionsController.CreateConditions(null, benefitType_db.BenefitTypeId, benefitTypeViewModel.Condition.ToList());
                }

                return RedirectToAction("Index", new { applicationId = benefitTypeViewModel.ApplicationId });
            }

            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", benefitTypeViewModel.ApplicationId);

            return View("BenefitTypeForm", benefitTypeViewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BenefitType benefitType_db = db.BenefitType.Find(id);

            if (benefitType_db == null)
            {
                return HttpNotFound();
            }

            BenefitTypeViewModel benefitTypeviewModel = Mapper.Map<BenefitTypeViewModel>(benefitType_db);

            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", benefitType_db.ApplicationId);

            return View("BenefitTypeForm", benefitTypeviewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BenefitTypeId,ApplicationId,Name,DisplayName,Text,MonthlyRate,MonthlyWorkHours,MaxLengthMonths,HourlyRate,Condition")] BenefitTypeViewModel benefitTypeViewModel)
        {
            if (ModelState.IsValid)
            {
                var benefitType_db = Mapper.Map<BenefitType>(benefitTypeViewModel);

                benefitType_db.Text = benefitType_db.Text == "<p><br></p>" ? null : benefitType_db.Text; //I IE gemmes "<p><br></p>" hvis Tekst-feltet er tomt derfor erstatter vi det med NULL

                db.Entry(benefitType_db).State = EntityState.Modified;
                db.SaveChanges();

                if (benefitTypeViewModel.Condition != null)
                {
                    ConditionsController.CreateConditions(null, benefitType_db.BenefitTypeId, benefitTypeViewModel.Condition.ToList());
                }
                else
                {
                    ConditionsController.DeleteConditions(null, benefitType_db.BenefitTypeId);
                }

                return RedirectToAction("Index", new { applicationId = benefitTypeViewModel.ApplicationId });
            }

            ViewBag.Applications = new SelectList(db.Application, "ApplicationId", "Name", benefitTypeViewModel.ApplicationId);

            return View("BenefitTypeForm", benefitTypeViewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
