﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ydelsesberegner.User.ViewModels
{
    public class AttachmentViewModel
    {
        public int AttachmentId { get; set; }

        [Display(Name = "Slide")]
        public int SlideId { get; set; }

        [Display(Name = "Filnavn")]
        public string FileName { get; set; }

        [Display(Name = "Filtype")]
        public string FileType { get; set; }

        [Display(Name = "Fil")]
        public byte[] FileContent { get; set; }

        public SlideViewModel Slide { get; set; }
    }
}