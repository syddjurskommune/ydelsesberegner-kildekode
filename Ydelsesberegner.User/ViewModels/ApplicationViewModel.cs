﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ydelsesberegner.User.ViewModels
{
    public class ApplicationViewModel
    {
        public int ApplicationId { get; set; }

        [Display(Name = "Navn")]
        public string Name { get; set; }

        [Display(Name = "Administratorgruppe")]
        public string AdminGroup { get; set; }

        [Display(Name = "Sti til resultat view")]
        public string ResultViewPath { get; set; }

        [Display(Name = "Action navn")]
        public string ActionName { get; set; }

        public string Layout { get; set; }
    }
}