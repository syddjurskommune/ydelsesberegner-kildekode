﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ydelsesberegner.User.ViewModels
{
    public class QuestionViewModel
    {
        public int QuestionId { get; set; }

        [Display(Name = "Slide")]
        public int SlideId { get; set; }

        [Display(Name = "Spørgsmål - Systemnavn")]
        public string QuestionSystemName { get; set; }

        [Display(Name = "Spørgsmål")]
        public string QuestionText { get; set; }

        [Display(Name = "Svarmuligheder")]
        public ICollection<AnswerViewModel> Answer { get; set; }
    }
}