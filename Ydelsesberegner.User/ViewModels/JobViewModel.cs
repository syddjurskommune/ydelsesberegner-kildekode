﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ydelsesberegner.User.ViewModels
{
    public class JobViewModel
    {
        public int JobId { get; set; }

        [Display(Name = "Applikation")]
        public int ApplicationId { get; set; }

        [Display(Name = "Job")]
        public string JobTitle { get; set; }

        [Display(Name = "Månedsløn")]
        public decimal? MonthlySalary { get; set; }

        [Display(Name = "Arbejdstimer månedligt")]
        public decimal? MonthlyWorkHours { get; set; }

        [Display(Name = "Timeløn")]
        public decimal? HourlySalary
        {
            get { return MonthlySalary.HasValue && MonthlyWorkHours.HasValue ? Math.Round(MonthlySalary.Value / MonthlyWorkHours.Value, 2) : 0; }
            private set { }
        }

        [Display(Name = "Årsløn")]
        public decimal? YearlySalary
        {
            get { return MonthlySalary.HasValue ? Math.Round(MonthlySalary.Value * 12, 2) : 0; }
            private set { }
        }
    }
}