﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ydelsesberegner.User.ViewModels
{
    public class AnswerViewModel
    {
        public int AnswerId { get; set; }

        [Display(Name = "Spørgsmål")]
        public int QuestionId { get; set; }

        [Display(Name = "Svar")]
        public string AnswerText { get; set; }
    }
}