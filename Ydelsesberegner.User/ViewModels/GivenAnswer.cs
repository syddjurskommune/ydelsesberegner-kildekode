﻿using Ydelsesberegner.User.Models;

namespace Ydelsesberegner.User.ViewModels
{
    public class GivenAnswer
    {
        public Question Question { get; set; }
        public Answer Answer { get; set; }
        public Slide Slide { get; set; }
        public string SlideTextBoxValue { get; set; }
    }
}