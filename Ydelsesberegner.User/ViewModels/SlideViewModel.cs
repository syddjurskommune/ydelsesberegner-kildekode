﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ydelsesberegner.User.ViewModels
{
    public class SlideViewModel
    {
        public int SlideId { get; set; }

        [Display(Name = "Applikation")]
        public int ApplicationId { get; set; }

        [Display(Name = "Sorteringsnøgle")]
        public int SlideOrderIndex { get; set; }

        [Display(Name = "Slide navn")]
        public string SlideName { get; set; }

        [Display(Name = "Slide overskrift")]
        public string SlideHeadline { get; set; }

        [Display(Name = "Slide tekst")]
        public string SlideText { get; set; }

        [Display(Name = "Resultat slide")]
        public bool IsResultSlide { get; set; }

        [Display(Name = "Vis Print-knap")]
        public bool DisplayPrintButton { get; set; }

        [Display(Name = "Vis Næste-knap")]
        public bool DisplayNextButton { get; set; }

        [Display(Name = "Vis Gå tilbage til start-knap")]
        public bool DisplayReturnToStartButton { get; set; }

        [Display(Name = "Vis tekstboks")]
        public bool DisplayTextBox { get; set; }

        [Display(Name = "Spørgsmål")]
        public QuestionViewModel Question { get; set; }

        [Display(Name = "Filer")]
        public ICollection<AttachmentViewModel> Attachment { get; set; }
    }
}