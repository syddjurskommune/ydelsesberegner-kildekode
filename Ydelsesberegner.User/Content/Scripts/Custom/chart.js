﻿var data = $("#chart").data("chart-data");
var labels = $("#chart").data("chart-labels").split(",");
var ctx = document.getElementById("chart").getContext("2d");
var chart = new Chart(ctx, {
    type: "bar",
    data: {
        labels: labels,
        datasets: [
            {
                backgroundColor: "rgb(63, 109, 159)",
                borderColor: "rgb(0, 0, 0)",
                data: data
            }
        ]
    },
    options: {
        legend: { display: false },
        responsive: true,
        scales: {
            yAxes: [{ ticks: { beginAtZero: true, fontSize: 14, callback: function (value, index, values) { return value + "kr."; } } }],
            xAxes: [{ ticks: { fontSize: 12 } }]
        }
    }
});