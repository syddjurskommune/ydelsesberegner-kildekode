﻿//Håndterer klik på højre (39) og venstre (37) piletast
$(document).ready($("html").keydown(function (e) {
    if (e.which === 39) {
        e.preventDefault();
        if (!$("#next-button").hasClass("hidden")) {
            $("#next-button").click();
        }
    } else if (e.which === 37) {
        e.preventDefault();
        if (!$("#back-button").hasClass("hidden")) {
            document.getElementById("back-button").click();
        }
    }
}));

//Klik på print
$("#print").click(function (e) {
    e.preventDefault();

    var printAction = $(this).data("print-action");
    var title = $("#page-title").val();
    var printUrl = encodeURI("/Print/" + printAction + "?title=" + title);
    var slideTextBoxValue = $("#SlideTextBox").val();

    $.ajax({
        type: "POST",
        url: "/Print/SaveTextBoxAnswer",
        data: { 'slideTextBoxValue': slideTextBoxValue },
        success: function () {
            window.open(printUrl, "", "menubar=0,location=0,resizable=1,scrollbars=1,height=700,width=950");
            $('#modal-page-title').modal('hide');
        },
        error: function (error) {
            console.log("Der opstod en fejl", error);
        }
    });
})