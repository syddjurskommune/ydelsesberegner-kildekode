﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ydelsesberegner.User.Controllers
{
    public class PrintController : Controller
    {
        public ActionResult Ydelsesberegner()
        {
            var history = (List<Models.Slide>)Session["History"];
            var currentSlide = (Models.Slide)Session["CurrentSlide"];

            if (!history.Contains(currentSlide))
            {
                history.Add(currentSlide);
            }

            ViewData["ApplicationName"] = "Ydelsesberegner";

            return View(history);
        }

        public void SaveTextBoxAnswer(string slideTextBoxValue)
        {
            var currentSlide = (Models.Slide)Session["CurrentSlide"];
            var givenAnswers = (List<ViewModels.GivenAnswer>)Session["GivenAnswers"];

            if (!string.IsNullOrWhiteSpace(slideTextBoxValue))
            {
                givenAnswers.Add(new ViewModels.GivenAnswer { Slide = currentSlide, SlideTextBoxValue = slideTextBoxValue });
            }
        }
    }
}