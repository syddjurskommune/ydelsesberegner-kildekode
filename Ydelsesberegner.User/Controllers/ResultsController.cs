﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.ApplicationInsights.Web;
using Ydelsesberegner.User.Models;
using Ydelsesberegner.User.ViewModels;

namespace Ydelsesberegner.User.Controllers
{
    public class ResultsController : Controller
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public ActionResult Ydelsesberegner(int slideId)
        {
            var slide = db.Slide.SingleOrDefault(s => s.SlideId == slideId);

            if (slide == null) return HttpNotFound();

            var slideViewModel = Mapper.Map<SlideViewModel>(slide);
            var benefitType = GetBenefitType(slide.ApplicationId);
            var jobs = db.Job.Where(j => j.ApplicationId == slideViewModel.ApplicationId).ToList();
            var jobsList = jobs.Aggregate("<ul>", (current, job) => current + $"<li>{GetWeeklyWorkHoursInJobComparedToSocialSecurity(job.JobId, benefitType.MonthlyRate)} timer om ugen til ca. {job.HourlySalary:C} ({job.JobTitle})</li>") + "<ul>";

            jobs.Add(new Job { JobTitle = benefitType.DisplayName, HourlySalary = benefitType.HourlyRate });

            var jobsJavaScript = string.Join(",", jobs.Select(j => j.JobTitle));

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            var hourlySalaryJavaScript = serializer.Serialize(jobs.Select(j => j.HourlySalary).ToArray());
            var text = !string.IsNullOrWhiteSpace(benefitType.Text) ? benefitType.Text : slideViewModel.SlideText;

            text = text.Replace("{", "{{")
                       .Replace("}", "}}")
                       .Replace("[BenefitType]", !string.IsNullOrWhiteSpace(benefitType.DisplayName) ? benefitType.DisplayName : benefitType.Name)
                       .Replace("[BenefitTypeMonthlyRate]", $"{benefitType.MonthlyRate:C}")
                       .Replace("[BenefitTypeHourlyRate]", $"{benefitType.HourlyRate:C}")
                       .Replace("[Jobs]", jobsList)
                       .Replace("[Chart]", "<canvas id=\"chart\" data-chart-data=\"" + hourlySalaryJavaScript + "\" data-chart-labels=\"" + jobsJavaScript + "\"></canvas>");

            var application = db.Application.Single(a => a.ApplicationId == slideViewModel.ApplicationId);

            ViewBag.Layout = application.Layout;
            ViewBag.PrintActionName = application.PrintActionName;
            ViewBag.Text = text;

            slide.SlideText = text;

            Session["CurrentSlide"] = slide;

            return View(slideViewModel);
        }

        protected BenefitTypeViewModel GetBenefitType(int applicationId)
        {
            var answers = (List<GivenAnswer>)Session["GivenAnswers"];

            if (answers != null)
            {
                foreach (var benefitType in db.BenefitType.Where(b => b.ApplicationId == applicationId).ToList())
                {
                    //Finder conditions ud fra SocialSecurityTypeId og gruppere efter QuestionID
                    var conditionsGroupedByQuestion = from condition in db.Condition
                                                      where condition.BenefitTypeId == benefitType.BenefitTypeId
                                                      group condition by condition.QuestionId into groupedConditions
                                                      orderby groupedConditions.Key
                                                      select groupedConditions;

                    bool isConditionsMet = false;

                    //Løber alle de fundne conditions igennem
                    foreach (var conditionGroup in conditionsGroupedByQuestion)
                    {
                        if (conditionGroup.Count() == 1) //Hvis der kun findes en condition på spørgsmålet
                        {
                            if (answers.Any(a => a.Answer.AnswerId == conditionGroup.Single().AnswerId)) //Tjekker om det AnswerID som står på Conditionen findes i blandt de svar der er afgivet gennem forløbet
                            {
                                isConditionsMet = true;
                                continue;
                            }
                            else
                            {
                                isConditionsMet = false;
                                break; //Stopper foreach loop eftersom conditionen ikke opfyldt
                            }
                        }

                        if (conditionGroup.Count() > 1) //Hvis der findes flere conditions på spørgsmålet
                        {
                            List<bool> conditionsInGroupMet = new List<bool>();

                            foreach (var condition in conditionGroup) //Løber hver condition igennem som findes på spørgsmål
                            {
                                bool isConditionMet = answers.Any(a => a.Answer.AnswerId == condition.AnswerId); //Tjekker om det AnswerID som står på Conditionen findes i blandt de svar der er afgivet gennem forløbet

                                conditionsInGroupMet.Add(isConditionMet); //Hvis svaret findes tilføjet vi True til listen ConditionsInGroupMet
                            }

                            if (conditionsInGroupMet.Any(c => c == true)) //Hvis bare er én condition tilhørende spørgsmålet er opfyldt så er conditionMet
                            {
                                isConditionsMet = true;
                            }
                            else
                            {
                                isConditionsMet = false; //Hvis ingen af de conditions som er tilknyttet spørgsmålet er opfyldt så afbrydes løkken
                                break;
                            }
                        }
                    }

                    if (isConditionsMet)
                    {
                        return Mapper.Map<BenefitTypeViewModel>(benefitType);
                    }
                }
            }

            return null;
        }
        protected string GetWeeklyWorkHoursInJobComparedToSocialSecurity(int jobId, decimal monthlyRate)
        {
            string weeklyWorkHours;

            var job = db.Job.SingleOrDefault(j => j.JobId == jobId);

            if (job != null)
            {
                var employmentAllowance = db.EmploymentAllowance.Single(e => e.Year == DateTime.Now.Year);

                decimal labourMarketContributions = (job.MonthlySalary.Value / 100 * 8); //Arbejdsmarkedsbidrag
                decimal monthlySalaryMinusLabourMarketContributions = job.MonthlySalary.Value - labourMarketContributions; //Månedsløn minus arbejdsmarkedsbidrag
                decimal employmentAllowanceCalculation = monthlySalaryMinusLabourMarketContributions / 100 * employmentAllowance.Percentage; //Beskæftigelsesfradrag
                decimal valueOfEmploymentAllowance = employmentAllowanceCalculation / 100 * employmentAllowance.TaxValue; //Værdi af beskæftigelsesfradrag
                decimal monthlySalaryAfterCalculations = monthlySalaryMinusLabourMarketContributions + valueOfEmploymentAllowance; //Månedsløn efter udregninger
                decimal hourlySalaryAfterCalculations = monthlySalaryAfterCalculations / job.MonthlyWorkHours.Value; //Timeløn efter udregninger
                decimal monthlyWorkHours = monthlyRate / hourlySalaryAfterCalculations; //Arbejdstimer månedligt
                decimal weeklyWorkHoursRounded = Math.Round(monthlyWorkHours / Convert.ToDecimal(4.33));

                weeklyWorkHours = weeklyWorkHoursRounded.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                weeklyWorkHours = "<i>Fejl: Job blev ikke fundet ud fra Job ID</i>";
            }

            return weeklyWorkHours;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}