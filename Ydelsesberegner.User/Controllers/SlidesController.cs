﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Ydelsesberegner.User.Models;
using Ydelsesberegner.User.ViewModels;

namespace Ydelsesberegner.User.Controllers
{
    public class SlidesController : Controller
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public ActionResult Slideshow(int? id)
        {
            if (Session["CurrentApplicationId"] == null)
            {
                RedirectToAction("Index", "Applications");
            }

            int applicationId = Convert.ToInt32(Session["CurrentApplicationId"]);
            bool displayDefaultSlide = Session["CurrentSlide"] == null || id == null;
            var slide = displayDefaultSlide ? db.Slide.SingleOrDefault(s => s.ApplicationId == applicationId && s.SlideOrderIndex == 0) : db.Slide.SingleOrDefault(s => s.ApplicationId == applicationId && s.SlideId == id);
            var answers = (List<GivenAnswer>)Session["GivenAnswers"];

            if (slide == null)
            {
                return HttpNotFound();
            }

            //Finder conditions ud fra SlideId og gruppere efter QuestionId
            var conditionsGroupedByQuestion = from condition in db.Condition
                                              where condition.SlideId == id && condition.BenefitTypeId == null
                                              group condition by condition.QuestionId into groupedConditions
                                              orderby groupedConditions.Key
                                              select groupedConditions;

            bool isConditionsMet = false;

            //Løber alle de fundne Conditions igennem
            foreach (var conditionGroup in conditionsGroupedByQuestion)
            {
                if (conditionGroup.Count() == 1) //Hvis der kun findes en Condition på spørgsmålet
                {
                    int answerId = conditionGroup.Single().AnswerId;

                    if (answers != null && answers.Any(a => a.Answer != null && a.Answer.AnswerId == answerId)) //Tjekker om det AnswerId som står på Conditionen findes i blandt de svar der er afgivet gennem forløbet
                    {
                        isConditionsMet = true;
                        continue;
                    }
                    else
                    {
                        isConditionsMet = false;
                        break; //Stopper foreach loop eftersom conditionen ikke opfyldt
                    }
                }

                if (conditionGroup.Count() > 1) //Hvis der findes flere Conditions på spørgsmålet
                {
                    List<bool> conditionsInGroupMet = new List<bool>();

                    foreach (var condition in conditionGroup) //Løber hver Condition igennem som findes på spørgsmål
                    {
                        bool isConditionMet = answers.Any(a => a.Answer != null && a.Answer.AnswerId == condition.AnswerId); //Tjekker om det AnswerId som står på Conditionen findes i blandt de svar der er afgivet gennem forløbet

                        conditionsInGroupMet.Add(isConditionMet); //Hvis svaret findes tilføjer vi True til listen ConditionsInGroupMet
                    }

                    if (conditionsInGroupMet.Any(c => c == true)) //Hvis bare er én Condition tilhørende spørgsmålet er opfyldt så er isConditionsMet = true
                    {
                        isConditionsMet = true;
                    }
                    else
                    {
                        isConditionsMet = false; //Hvis ingen af de Conditions som er tilknyttet spørgsmålet er opfyldt så afbrydes løkken
                        break;
                    }
                }
            }

            bool displaySlide = !conditionsGroupedByQuestion.Any() || slide.IsResultSlide && (isConditionsMet || !conditionsGroupedByQuestion.Any()) || isConditionsMet;

            if (displaySlide)
            {
                var application = db.Application.Single(a => a.ApplicationId == slide.ApplicationId);

                ViewBag.Layout = application.Layout;
                ViewBag.PrintActionName = application.PrintActionName;
                ViewBag.ApplicationName = application.Name;

                Session["CurrentSlide"] = slide;

                if (slide.IsResultSlide)
                {
                    return RedirectToAction(application.ActionName, "Results", new { slideId = slide.SlideId });
                }

                var slideViewModel = Mapper.Map<SlideViewModel>(slide);

                return View(slideViewModel);
            }
            else
            {
                return GoToNextSlideByCurrentSlideOrderIndex(slide.SlideOrderIndex);
            }
        }
      
        public ActionResult GoToPreviousSlide()
        {
            List<Models.Slide> history = (List<Models.Slide>)Session["History"];

            var previousSlide = history.Last();

            DeleteSlideFromHistory(previousSlide);

            //Hvis der er afgivet et svar på slidet fjernes det fra GivenAnswer sessionen
            List<GivenAnswer> givenAnswers = (List<GivenAnswer>)Session["GivenAnswers"];

            if (givenAnswers != null && givenAnswers.Count > 0)
            {
                int currentSlideId = ((Models.Slide)Session["CurrentSlide"]).SlideId;

                givenAnswers.RemoveAll(ga => ga.Slide.SlideId == currentSlideId);

                Session["GivenAnswers"] = givenAnswers;
            }

            return RedirectToAction("Slideshow", "Slides", new { id = previousSlide.SlideId });
        }
        public ActionResult GoToNextSlide(int currentSlideOrderIndex, int? questionId, int? answerId, string slideTextBox)
        {
            var currentSlide = (Models.Slide)Session["CurrentSlide"];

            SaveSlideToHistory(currentSlide);

            List<GivenAnswer> givenAnswers = Session["GivenAnswers"] == null ? new List<GivenAnswer>() : (List<GivenAnswer>)Session["GivenAnswers"];

            givenAnswers.RemoveAll(ga => ga.Slide.SlideId == currentSlide.SlideId); //Sletter alle svar som er afgivet på dette slide tidligere

            if (questionId != null && answerId != null || !string.IsNullOrWhiteSpace(slideTextBox))
            {
                var givenAnswer = new GivenAnswer { Slide = currentSlide };

                if (questionId != null && answerId != null)
                {
                    Question question = db.Question.Single(q => q.QuestionId == questionId);
                    Answer answer = db.Answer.Single(a => a.AnswerId == answerId);

                    givenAnswer.Question = question;
                    givenAnswer.Answer = answer;
                }

                if (!string.IsNullOrWhiteSpace(slideTextBox))
                {
                    givenAnswer.SlideTextBoxValue = slideTextBox;
                }

                givenAnswers.Add(givenAnswer);
            }

            Session["GivenAnswers"] = givenAnswers;

            return GoToNextSlideByCurrentSlideOrderIndex(currentSlideOrderIndex);
        }
        public ActionResult GoToNextSlideByCurrentSlideOrderIndex(int currentSlideOrderIndex)
        {
            if (Session["CurrentApplicationId"] == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int applicationId = Convert.ToInt32(Session["CurrentApplicationId"]);
            var nextSlide = db.Slide.SingleOrDefault(s => s.ApplicationId == applicationId && s.SlideOrderIndex == currentSlideOrderIndex + 1);

            if (nextSlide == null)
            {
                return HttpNotFound();
            }

            return RedirectToAction("Slideshow", "Slides", new { id = nextSlide.SlideId });
        }
        public ActionResult GoToStart()
        {
            Session.Remove("History");
            Session.Remove("GivenAnswers");
            Session.Remove("CurrentSlide");
            
            return RedirectToAction("Slideshow");
        }
        
        protected void SaveSlideToHistory(Models.Slide slide)
        {
            List<Models.Slide> history = Session["History"] == null ? new List<Models.Slide>() : (List<Models.Slide>)Session["History"];

            if (!history.Contains(slide))
            {
                history.Add(slide);
            }
            else
            {
                history.Remove(slide);
            }

            Session["History"] = history;
        }
        protected void DeleteSlideFromHistory(Models.Slide slide)
        {
            List<Models.Slide> history = (List<Models.Slide>)Session["History"];

            if (history == null) return;

            history.Remove(slide);

            Session["History"] = history;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}