﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ydelsesberegner.User.Models;

namespace Ydelsesberegner.User.Controllers
{
    public class AttachmentsController : Controller
    {
        public ActionResult DownloadAttachment(int attachmentId)
        {
            using (var db = new YdelsesberegnerContext())
            {
                var attachment = db.Attachment.SingleOrDefault(a => a.AttachmentId == attachmentId);

                if (attachment == null)
                {
                    return HttpNotFound();
                }

                byte[] fileBytes = attachment.FileContent;
                string fileName = attachment.FileName;

                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
        }
    }
}