﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ydelsesberegner.User.Models;
using Ydelsesberegner.User.ViewModels;

namespace Ydelsesberegner.User.Controllers
{
    public class ApplicationsController : Controller
    {
        private readonly YdelsesberegnerContext db = new YdelsesberegnerContext();

        public ActionResult Index()
        {
            var url = Request.Url;
            var applications = new List<Application>();

            applications.AddRange(db.Application.ToList().Where(app => url.AbsoluteUri == app.Url));

            switch (applications.Count)
            {
                case 0:
                    return HttpNotFound("Der blev ikke fundet nogle beregnere ud fra URL'en");
                case 1:
                    return RedirectToAction("ChooseApplication", "Applications", new { id = applications.Single().ApplicationId });
                default:
                    var applicationsViewModel = Mapper.Map<List<Application>, List<ApplicationViewModel>>(applications);

                    ViewBag.Layout = applications.Select(a => a.Layout).Distinct().Count() == 1 ? applications.First().Layout : "~/Views/Shared/Layouts/_DefaultLayout.cshtml";

                    return View(applicationsViewModel);
            }
        }

        public ActionResult ChooseApplication(int? id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            var application = db.Application.SingleOrDefault(a => a.ApplicationId == id);

            if (application == null) { return HttpNotFound(); }

            Session["CurrentApplicationId"] = application.ApplicationId;

            return RedirectToAction("Slideshow", "Slides");
        }

        public ActionResult ClearSessions()
        {
            Session.Remove("History");
            Session.Remove("GivenAnswers");
            Session.Remove("CurrentSlide");
            Session.Remove("TemporaryAnswers");
            Session.Remove("TemporaryConditions");

            return Session["CurrentApplicationId"] == null ? RedirectToAction("Index") : RedirectToAction("Slideshow", "Slides");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}