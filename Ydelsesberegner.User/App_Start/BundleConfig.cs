﻿using System.Web;
using System.Web.Optimization;

namespace Ydelsesberegner.User
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/Scripts/Libraries/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/Scripts/Libraries/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/Scripts/Libraries/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/Scripts/Libraries/bootstrap.js",
                      "~/Content/Scripts/Libraries/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Content/Scripts/Libraries/angular/angular.js",
                "~/Content/Scripts/Libraries/angular/i18n/angular-locale_da-dk.js",
                "~/Content/Scripts/Libraries/angular/angular-mocks.js",
                "~/Content/Scripts/Libraries/angular/angular-messages.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-app").Include(
                "~/Content/Scripts/Angular/app.js",
                "~/Content/Scripts/Angular/services/filters.js",
                "~/Content/Scripts/Angular/services/directives.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/Styles/Libraries/bootstrap.min.css", new CssRewriteUrlTransform())
                                                        .Include("~/Content/Styles/Custom/site.css"));
        }
    }
}
