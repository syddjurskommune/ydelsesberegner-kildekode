﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace Ydelsesberegner.User
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Models.Application, ViewModels.ApplicationViewModel>();

                cfg.CreateMap<Models.Slide, ViewModels.SlideViewModel>().ForMember(x => x.Question, opt => opt.MapFrom(src => src.Question.FirstOrDefault()));

                cfg.CreateMap<Models.Question, ViewModels.QuestionViewModel>();

                cfg.CreateMap<Models.Attachment, ViewModels.AttachmentViewModel>();

                cfg.CreateMap<Models.Answer, ViewModels.AnswerViewModel>();

                cfg.CreateMap<Models.BenefitType, ViewModels.BenefitTypeViewModel>();

                cfg.CreateMap<Models.Job, ViewModels.JobViewModel>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            });
        }
    }
}
